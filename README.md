# Beyond Vanilla - Private FTB Revelation Server

## Mods
See mods folder <I'll be adding a page for this in the future>

## Changle Log
See changelog.json

# Instructions

1. Download the [latest release](https://gitlab.com/scottyedmonds/beyondvanilla_ftb/-/releases)
2. Install FTB Revelation however you want
3. Delete the mods folder and copy the one downloaded in the release into your Minecraft folder
4. Relaunch Minecraft
5. Join Server


If you have any questions - joint our discord server: [https://discord.gg/MSVFHBz](https://discord.gg/MSVFHBz)


# FAQ

The server is whitelisted, so it's not a free for all
